﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot
{
    public class Car : Vehicle
    {
        public override int Size
        {
            get { return 1; }
        }
        public override int Cost
        {
            get { return 5;  }
        }
        public override string Type
        {
            get { return "Car"; }
        }
    }
}
