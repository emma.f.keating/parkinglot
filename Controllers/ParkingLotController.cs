﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ParkingLot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParkingLotController : ControllerBase
    {
        private static readonly string[] mVehicleTypes = new[]
        {
            "Car", "Monster Truck"
        };
        private static ParkingLotManager mParkingLotManager;

        public ParkingLotController()
        {
            if(mParkingLotManager == null)
            {
                List<int> parkingBlockSizes = new List<int> { 3, 4, 5, 4, 3 };
                mParkingLotManager = new ParkingLotManager(parkingBlockSizes);
            }
        }

        [HttpGet]
        public Report Get()
        {
            Report report = new Report();
            report.MoneyCollected = mParkingLotManager.MoneyCollected();
            report.VehiclesParked = new List<Tuple<string, int>>();
            foreach(string vehicleType in mVehicleTypes)
            {
                Tuple<string, int> vehicleData = new Tuple<string, int>(vehicleType, mParkingLotManager.VehicleCountByType(vehicleType));
                report.VehiclesParked.Add(vehicleData);
            }
            return report;
        }

        [HttpPost]
        public bool Post([FromBody] string vehicleType)
        {
            Vehicle vehicle;
            switch(vehicleType.ToLower())
            {
                case "car":
                    vehicle = new Car();
                    break;
                case "monster truck":
                    vehicle = new MonsterTruck();
                    break;
                default:
                    throw new Exception("Error: Vehicle Type Not Found");
            }

            return mParkingLotManager.ParkVehicle(vehicle);
        }
    }
}
