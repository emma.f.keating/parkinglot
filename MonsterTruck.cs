﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot
{
    public class MonsterTruck : Vehicle
    {
        public override int Size
        {
            get { return 2; }
        }
        public override int Cost
        {
            get { return 15; }
        }
        public override string Type
        {
            get { return "Monster Truck"; }
        }
    }
}
