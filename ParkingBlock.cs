﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot
{
    public class ParkingBlock
    {
        private List<ParkingSpace> mParkingSpaces;

        public ParkingBlock(int numberOfSpaces)
        {
            mParkingSpaces = new List<ParkingSpace>();
            for(int parkingSpaceIndex = 0; parkingSpaceIndex < numberOfSpaces; parkingSpaceIndex++)
            {
                ParkingSpace parkingSpace = new ParkingSpace();
                parkingSpace.Occupied = false;
                mParkingSpaces.Add(parkingSpace);
            }
        }

        public bool SpaceAvailable(Vehicle vehicle)
        {
            int biggestSpaceSize = 0;
            int currentSpaceSize = 0;
            foreach (ParkingSpace parkingSpace in mParkingSpaces)
            {
                if (parkingSpace.Occupied == false)
                {
                    currentSpaceSize++;
                }
                else
                {
                    if(currentSpaceSize > biggestSpaceSize)
                    {
                        biggestSpaceSize = currentSpaceSize;
                    }
                    currentSpaceSize = 0;
                }
            }

            if (currentSpaceSize > biggestSpaceSize)
            {
                biggestSpaceSize = currentSpaceSize;
            }

            return biggestSpaceSize >= vehicle.Size;
        }

        public void ParkVehicle(Vehicle vehicle)
        {
            List<int> availableParkingSpaceIndexes = new List<int>();
            for (int parkingSpaceIndex = 0; parkingSpaceIndex < mParkingSpaces.Count(); parkingSpaceIndex++)
            {
                if (mParkingSpaces[parkingSpaceIndex].Occupied == false)
                {
                    bool spaceAvailable = true;
                    for (int adjacentSpaceIndex = 1; adjacentSpaceIndex < vehicle.Size; adjacentSpaceIndex++)
                    {
                        int adjacentSpace = parkingSpaceIndex + adjacentSpaceIndex;
                        if (adjacentSpace >= mParkingSpaces.Count || mParkingSpaces[adjacentSpace].Occupied)
                        {
                            spaceAvailable = false;
                            break;
                        }
                    }

                    if(spaceAvailable)
                    {
                        availableParkingSpaceIndexes.Add(parkingSpaceIndex);
                    }
                }
            }

            if(availableParkingSpaceIndexes.Count == 0)
            {
                throw new Exception("Error: No Space Available in Parking Block");
            }

            Random rand = new Random();
            int selectedSpaceIndex = rand.Next(availableParkingSpaceIndexes.Count);
            int selectedSpace = availableParkingSpaceIndexes[selectedSpaceIndex];
            int maxSelectedSpaceIndex = selectedSpace + vehicle.Size;
            for (int parkingSpaceIndex = selectedSpace; parkingSpaceIndex < maxSelectedSpaceIndex; parkingSpaceIndex++)
            {
                if (parkingSpaceIndex >= mParkingSpaces.Count)
                {
                    throw new Exception("Error: Parking Space Index Out of Bounds");
                }

                if(mParkingSpaces[parkingSpaceIndex].Occupied)
                {
                    throw new Exception("Error: Parking Space Already Occupied");
                }

                mParkingSpaces[parkingSpaceIndex].Occupied = true;
                mParkingSpaces[parkingSpaceIndex].ParkedVehicle = vehicle;
            }
        }
    }
}
