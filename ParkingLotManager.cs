﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingLot
{
    public class ParkingLotManager
    {
        private static Mutex mMutex;
        private List<Vehicle> mParkedVehicles;
        private List<ParkingBlock> mParkingBlocks;

        public ParkingLotManager(List<int> parkingBlockSizes)
        {
            mMutex = new Mutex();
            mParkedVehicles = new List<Vehicle>();
            mParkingBlocks = new List<ParkingBlock>();
            foreach(int parkingBlockSize in parkingBlockSizes)
            {
                mParkingBlocks.Add(new ParkingBlock(parkingBlockSize));
            }
        }

        ~ParkingLotManager()
        {
            mMutex.Dispose();
        }

        public bool ParkVehicle(Vehicle vehicle)
        {
            mMutex.WaitOne();
            List<ParkingBlock> mAvailableParkingBlocks = new List<ParkingBlock>();
            foreach(ParkingBlock parkingBlock in mParkingBlocks)
            {
                if(parkingBlock.SpaceAvailable(vehicle))
                {
                    mAvailableParkingBlocks.Add(parkingBlock);
                }
            }

            if(mAvailableParkingBlocks.Count > 0)
            {
                try
                {
                    Random rand = new Random();
                    int selectedIndex = rand.Next(mAvailableParkingBlocks.Count);
                    mAvailableParkingBlocks[selectedIndex].ParkVehicle(vehicle);
                    mParkedVehicles.Add(vehicle);
                }
                finally
                {
                    mMutex.ReleaseMutex();
                }
                return true;
            }
            else
            {
                mMutex.ReleaseMutex();
                return false;
            }
        }

        public int MoneyCollected()
        {
            mMutex.WaitOne();
            int moneyCollected = 0;
            if (mParkedVehicles.Count > 0)
            {
                moneyCollected = mParkedVehicles.Select(x => x.Cost).Sum();
            }
            mMutex.ReleaseMutex();
            return moneyCollected;
        }

        public int VehicleCountByType(string type)
        {
            mMutex.WaitOne();
            int vehicleCount = 0;
            if (mParkedVehicles.Count > 0)
            {
                vehicleCount = mParkedVehicles.Where(x => x.Type == type).Count();
            }
            mMutex.ReleaseMutex();
            return vehicleCount;
        }
    }
}
