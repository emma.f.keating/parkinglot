﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot
{
    public class ParkingSpace
    {
        public bool Occupied { get; set; }

        public Vehicle ParkedVehicle { get; set; }
    }
}
