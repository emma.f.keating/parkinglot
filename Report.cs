﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot
{
    public class Report
    {
        public int MoneyCollected { get; set; }
        public List<Tuple<string, int>> VehiclesParked { get; set; }
    }
}
