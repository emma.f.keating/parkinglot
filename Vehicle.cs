﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot
{
    public abstract class Vehicle
    {
        public abstract int Size { get; }
        public abstract int Cost { get; }
        public abstract string Type { get;  }
    }
}
